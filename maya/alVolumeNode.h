#pragma once

#include <extension/Extension.h>
#include <extension/ExtensionsManager.h>
#include <translators/shape/ShapeTranslator.h>

#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MString.h>
#include <maya/MDagPath.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MFnPlugin.h>
#include <maya/MDistance.h>
#include <maya/MMatrix.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MGlobal.h>

struct Geometry
{
    Geometry() : displayList(0) {}

    MPoint mn;
    MPoint mx;
    GLuint displayList;
    int previewStyle;
};

class alVolumeNode : public MPxSurfaceShape
{
public:
    alVolumeNode();
    virtual ~alVolumeNode();

    void postConstructor() {setRenderable(true);}

    virtual MStatus         compute( const MPlug& plug, MDataBlock& data );

    void* getGeometry();

    virtual bool            isBounded() const;
    virtual MBoundingBox    boundingBox() const;

    static  void *          creator();
    static  MStatus         initialize();

    static MObject          _aVolumePath;
    static MObject          _aResolvedVolumePath;
    std::string             _resolvedVolumePath;
    static MObject          _aDensityLayer;
    static MObject          _aTemperatureLayer;
    static MObject          _aDummy;
    static MObject          _aStepSize;

    static MObject          _aDensityRemap;
    static MObject          _aDensityInputMin;
    static MObject          _aDensityInputMax;
    static MObject          _aDensityClipMin;
    static MObject          _aDensityClipMax;
    static MObject          _aDensityBias;
    static MObject          _aDensityGain;
    static MObject          _aDensityOutputMin;
    static MObject          _aDensityOutputMax;

    static MObject          _aTemperatureRemap;
    static MObject          _aTemperatureInputMin;
    static MObject          _aTemperatureInputMax;
    static MObject          _aTemperatureClipMin;
    static MObject          _aTemperatureClipMax;
    static MObject          _aTemperatureBias;
    static MObject          _aTemperatureGain;
    static MObject          _aTemperatureOutputMin;
    static MObject          _aTemperatureOutputMax;


    static MObject          _aScattering;
    static MObject          _aAbsorption;
    static MObject          _aG;

    static MObject          _aEmissionStrength;
    static MObject          _aPhysicalIntensity;

    static MObject          _aTime;

    enum PreviewStyle
    {
        kBoundingBox = 0,
        kDensityPoints,
        kEmissionPoints
    };
    static MObject          _aPreviewStyle;

    bool _valid;
    MPoint                  _bbmin;
    MPoint                  _bbmax;
    MVector                 _voxelSize;

    Geometry                _geometry;

public:
    static  MTypeId     id;
    static  MString     drawDbClassification;
    static  MString     drawRegistrantId;
};

class alVolumeNodeUI : public MPxSurfaceShapeUI
{
public:
    virtual void draw(const MDrawRequest& drawRequest, M3dView& view) const;
    virtual void getDrawRequests(const MDrawInfo& info, bool objectAndActiveOnly, MDrawRequestQueue& queue);
    virtual bool select( MSelectInfo &selectInfo,
                            MSelectionList &selectionList,
                            MPointArray &worldSpaceSelectPts ) const;
    static void* creator();
protected:
};

class alVolumeNodeTranslator : public CShapeTranslator
{
public:
    static void* create();

    bool IsMayaTypeRenderable() { return true;}

protected:
    virtual void Export(AtNode* node);
    virtual void Update(AtNode* node);
    virtual AtNode* CreateArnoldNodes();
};