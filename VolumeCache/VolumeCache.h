#pragma once

#include <OpenEXR/ImathVec.h>
#include <map>
#include <Field3D/SparseField.h>
#include <Field3D/SparseFile.h>
#include <Field3D/Field3DFile.h>
#include <Field3D/FieldInterp.h>
#include <openvdb/openvdb.h>
#include <openvdb/tools/Interpolation.h>

#include <boost/thread/tss.hpp>

// some useful shorthands
typedef openvdb::FloatGrid FloatGridType;
typedef FloatGridType::TreeType FloatGridTreeType;

/// Abstract base class for cache instances. One CacheInstance represents a single layer from a Field3D or OpenVDB cache
class CacheInstance
{
public:
    enum CacheType
    {
        kField3D = 0,
        kOpenVDB
    };

    CacheInstance();
    virtual ~CacheInstance();

    /// Lookup an interpolated value from the cache
    /// @param wsP The world-space position to look up
    /// @return The field value at @wsP
    virtual float lookup(const Imath::V3d& wsP) const = 0;
    /// Get the world-space bounding-box of the field
    /// @param Bounding box minimum
    /// @param Bounding box maximum
    virtual void bounds(Imath::V3d& bbmin, Imath::V3d& bbmax) const = 0;
    /// Is the cache valid or not?
    inline bool valid() const {return _valid;}
    /// What type is the cache (either kField3D or kOpenVDB)
    inline CacheType cacheType() const {return _cacheType;}
    /// How much memory is this CacheInstance using?
    virtual size_t memUsage() const = 0;
    /// How big are the voxels?
    virtual Imath::V3d voxelSize() const = 0;

    typedef boost::shared_ptr<CacheInstance> Ptr;

protected:
    bool _valid;
    CacheType _cacheType;
};



/// The VolumeCache object manages loading particular layers from cache files and managing their lifetime in memory.
/// To use it callers should use VolumeCache::instance() to get the singleton instance, then call readFloatField() to read a layer from a cache
/// into memory. This returns a Handle object that can be used to get the actual CacheInstance object that stores the layer. If the returned
/// handle is less than 0 then something went wrong loading the layer. Attempting to get a CacheInstance from such a Handle will return a null 
/// pointer.
class VolumeCache
{
public:
    /// Handle for accessing loaded volume cache data
    typedef int Handle;
    static const int kInvalidFile = -1;
    static const int kInvalidField = -2;
    static const int kInvalidFieldType = -3;
    static const int kUnknownError = -9;

    enum FilterType
    {
        kFast = 0,
        kLinear,
        kSmooth
    };

    /// Get the volume cache manager singleton
    static VolumeCache* instance();

    /// Read a float field/grid from the given volume file
    /// @return A handle for accessing the grid data. If the read failed the returned Handle will have value < 0
    /// @param cacheName Path to the Field3D or OpenVDB file to read
    /// @param fieldName Name of the field in the cache to read
    Handle readFloatField(const std::string& cacheName, const std::string& fieldName);

    /// Evict a loaded field from memory. The object will be destroyed after any remaining Ptrs held by the caller are destroyed
    void evict(Handle h);

    /// Report the total size, in bytes, of the memory used by caches currently loaded
    size_t memUsage() const;

    /// Return the CacheInstance for the given Handle
    CacheInstance::Ptr getCacheInstance(Handle h);
    
private:
    VolumeCache() : _numEntries(0) {}
    static VolumeCache* _instance;

    struct StaticInit
    {
    public:
        StaticInit();
    };
    static StaticInit _staticInit;

    typedef std::map<std::string,Handle> FileHandleMap;
    FileHandleMap _fileHandleMap;

    typedef std::map<Handle,CacheInstance::Ptr> CacheInstanceMap;
    CacheInstanceMap _cacheInstanceMap;

    // load a field from a field3d file and create a new CacheInstance for it
    Handle readField3DField(const std::string& cacheName, const std::string& fieldName);

    // load a grid from an OpenVDB file and create a new CacheInstance for it
    Handle readOpenVDBGrid(const std::string& cacheName, const std::string& gridName);

    uint32_t _numEntries;

    friend class VolumeCache::StaticInit;
};

/// Field3D CacheInstance implementation
class Field3DCacheInstance : public CacheInstance
{
public:
    Field3DCacheInstance(Field3D::SparseField<float>::Ptr field, Field3D::MatrixFieldMapping::Ptr mapping);
    
    float lookup(const Imath::V3d& wsP) const;
    void bounds(Imath::V3d& bbmin, Imath::V3d& bbmax) const;
    size_t memUsage() const;
    Imath::V3d voxelSize() const;

    Field3D::SparseField<float>::Ptr _field; 
    Field3D::MatrixFieldMapping::Ptr _mapping;
    Field3D::SparseField<float>::LinearInterp _lint;

    typedef boost::shared_ptr<Field3DCacheInstance> Ptr;
};

/// OpenVDB CacheInstance implementation
class OpenVDBCacheInstance : public CacheInstance
{
public:
    OpenVDBCacheInstance(VolumeCache::Handle h, openvdb::FloatGrid::Ptr grid);
    float lookup(const Imath::V3d& wsP) const;
    void bounds(Imath::V3d& bbmin, Imath::V3d& bbmax) const;
    size_t memUsage() const;
    Imath::V3d voxelSize() const;

    typedef openvdb::tools::GridSampler<openvdb::FloatGrid::TreeType, openvdb::tools::BoxSampler> FloatGridSampler;

    FloatGridSampler::Ptr _gridSampler;

    openvdb::FloatGrid::Ptr _grid;
    VolumeCache::Handle _handle;

    typedef boost::shared_ptr<OpenVDBCacheInstance> Ptr;
};