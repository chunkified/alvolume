cmake_minimum_required(VERSION 2.8)

project(alVolume)

set(CMAKE_BUILD_TYPE RELEASE)
set(CMAKE_VERBOSE_MAKEFILE TRUE)
set(CMAKE_SKIP_RPATH TRUE)
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_FLAGS_DEBUG "-g")

# pre-set the arnold and mtoa versions and root paths
set(ARNOLD_VERSION 4.0.12.0)
set(ARNOLD_ROOT /Users/anders/vfx/arnold/${ARNOLD_VERSION})

set(MTOA_VERSION 0.22.1)
set(MTOA_ROOT /Users/anders/vfx/mtoa/${MTOA_VERSION})

set(PYTHON_ROOT /System/Library/Frameworks/Python.framework/Versions/2.7)

# check if we have a local cmake include file and include it if we do
# this is useful for overriding our arnold and mtoa locations
if(EXISTS ${CMAKE_SOURCE_DIR}/local.cmake)
    message(INFO " --- Using local.cmake")
    include(${CMAKE_SOURCE_DIR}/local.cmake)
else()
    message(INFO " --- No local.cmake found")
endif()

include_directories(${ARNOLD_ROOT}/include)
link_directories(${ARNOLD_ROOT}/bin)

set(MTOA_SHADERS ${MTOA_ROOT}/shaders)
set(MTOA_SCRIPTS ${MTOA_ROOT}/scripts)
set(MTOA_UI ${MTOA_ROOT}/scripts/mtoa/ui/ae)

set(ARNOLD_PLUGIN_PATH ${ARNOLD_ROOT}/plugins)

include_directories(common)

include_directories(${PYTHON_ROOT}/include/python2.7)
link_directories(${PYTHON_ROOT}/lib)

set(FIELD3D_ROOT /Users/anders/vfx/field3d/1.3.2)
set(BOOST_ROOT /Users/anders/vfx/boost/1.43.0)
set(HDF5_ROOT /Users/anders/vfx/hdf5/1.8.7)
set(OPENEXR_ROOT /Users/anders/vfx/openexr/1.7.1)
set(ILMBASE_ROOT /Users/anders/vfx/ilmbase/1.0.3)
set(OPENVDB_ROOT /Users/anders/vfx/openvdb/0.104.0)

include_directories(${OPENEXR_ROOT}/include)
include_directories(${ARNOLD_ROOT}/include)
include_directories(${FIELD3D_ROOT}/include)
include_directories(${BOOST_ROOT}/include)
include_directories(${HDF5_ROOT}/include)
include_directories(${OPENVDB_ROOT}/include)

link_directories(${ARNOLD_ROOT}/bin)
link_directories(${FIELD3D_ROOT}/lib)
link_directories(${HDF5_ROOT}/lib)
link_directories(${OPENVDB_ROOT}/lib)
link_directories(${BOOST_ROOT}/lib)
link_directories(${ILMBASE_ROOT}/lib)


set(SUBDIRECTORIES 
        arnold
        maya
        VolumeCache
)

foreach(SUBDIR ${SUBDIRECTORIES})
    add_subdirectory(${SUBDIR})
endforeach()

